#include <iostream>
#include <fstream>
#include <boost/property_tree/json_parser.hpp>

#include "src/io.h"
#include "src/subdivision.h"
#include "src/ptree.h"
#include "src/green_deformer.h"
#include "src/macro.h"
#include "src/timer.h"

using namespace std;
using namespace Eigen;
using namespace green;

static string outdir;
static GreenElasticDeformer gf;
static high_resolution_timer clk;
static int NUM_THREADS = 0;

static int basic_point_load(ptree &pt) {
  // surface mesh
  std::vector<char> ax = {'X', 'Y', 'Z'};
  for (size_t i = 0; i < ax.size(); ++i) {
    char infile[256];
    sprintf(infile, "../data/plane-%c.obj", ax[i]);
    mati_t quad; matd_t verts;
    ASSERT(read_wavefront_obj(infile, quad, verts) == 0);
    spdlog::info("cell num={}, vert num={}", quad.cols(), verts.cols());
    verts *= pt.get<double>("domain_scale.value");

    const double sf = 200;
    Vector3d f = Vector3d::Zero();
    f[ax[i]-'X'] = sf;
    Vector3d O = verts*vecd_t::Ones(verts.cols())/verts.cols();

    matd_t deform_nods = verts;

    clk.start();
    #pragma omp parallel for num_threads(NUM_THREADS)
    for (size_t i = 0; i < verts.cols(); ++i) {
      const Vector3d &u = verts.col(i)-O;
      double r, theta, phi;
      xyz_to_rpt(u.x(), u.y(), u.z(), r, phi, theta);     
      deform_nods.col(i) += gf.assembleG(r, theta, phi)*f;
    }
    clk.stop();
    spdlog::info("time duration={} ms", clk.duration());

    char outf[256];
    sprintf(outf, "%s/deform-%c.obj", outdir.c_str(), 'X'+char(i));
    save_wavefront_obj(outf, quad, deform_nods);

    // visualize solution from each band
    for (int l = 0; l <= gf.num_band(); l += 2) {
      matd_t deform_nods = verts;

      #pragma omp parallel for num_threads(NUM_THREADS)
      for (size_t i = 0; i < verts.cols(); ++i) {
        const Vector3d &u = verts.col(i)-O;
        double r, theta, phi;
        xyz_to_rpt(u.x(), u.y(), u.z(), r, phi, theta);     
        deform_nods.col(i) += gf.assembleG_l(l, r, theta, phi)*f;
      }
      
      char outf[256];
      sprintf(outf, "%s/deform-%c-band-%03d.obj", outdir.c_str(), 'X'+char(i), l);
      save_wavefront_obj(outf, quad, deform_nods);
    }
  }

  //-> write configuration
  ofstream json_ofs(outdir+"/config-green.json");
  boost::property_tree::write_json(json_ofs, pt, true);
  json_ofs.close();
  
  return 0;
}

static int count_over_degree(ptree &pt) {
  {
    char infile[256] = "../data/plane-Y.obj";
    mati_t quad; matd_t verts;
    ASSERT(read_wavefront_obj(infile, quad, verts) == 0);
    spdlog::info("cell num={}, vert num={}", quad.cols(), verts.cols());
    verts *= 0.5;

    Vector3d f(0, 200, 0);
    Vector3d O = verts*vecd_t::Ones(verts.cols())/verts.cols();
    
    matd_t deform_nods = verts;

    std::vector<double> msL;
    for (int l = 0; l <= gf.num_band(); l += 2) {
      matd_t deform_nods = verts;

      clk.start();      
      for (size_t i = 0; i < verts.cols(); ++i) {
        const Vector3d &u = verts.col(i)-O;
        double r, theta, phi;
        xyz_to_rpt(u.x(), u.y(), u.z(), r, phi, theta);     
        deform_nods.col(i) += gf.assembleG_l(l, r, theta, phi)*f;
      }
      clk.stop();
      msL.emplace_back(clk.duration()/verts.cols());
      
      char outf[256];
      sprintf(outf, "%s/deform-band-%03d.obj", outdir.c_str(), l);
      save_wavefront_obj(outf, quad, deform_nods);
    }

    ofstream ofs(outdir+"/vector-degree-time.csv");
    ofs << "degree, time" << endl;
    double total_ms = 0;    
    for (int l = 0; l < msL.size(); ++l) {
      total_ms += msL[l];
      spdlog::info("L={}, time={}", 2*l, total_ms);
      ofs << 2*l << "," << total_ms << endl;
    }
    ofs.close();
  }

  {
    // mesh
    mati_t cell(8, 1); matd_t nods(3, 8);
    {
      cell(0, 0) = 0; cell(1, 0) = 1; cell(2, 0) = 2; cell(3, 0) = 3;
      cell(4, 0) = 4; cell(5, 0) = 5; cell(6, 0) = 6; cell(7, 0) = 7;
      
      nods(0, 0) = -1;  nods(1, 0) = -1; nods(2, 0) = -1;
      nods(0, 1) = -1;  nods(1, 1) = +1; nods(2, 1) = -1;
      nods(0, 2) = +1;  nods(1, 2) = -1; nods(2, 2) = -1;
      nods(0, 3) = +1;  nods(1, 3) = +1; nods(2, 3) = -1;
      nods(0, 4) = -1;  nods(1, 4) = -1; nods(2, 4) = +1;
      nods(0, 5) = -1;  nods(1, 5) = +1; nods(2, 5) = +1;
      nods(0, 6) = +1;  nods(1, 6) = -1; nods(2, 6) = +1;
      nods(0, 7) = +1;  nods(1, 7) = +1; nods(2, 7) = +1;
    }
    int subd_times = 4;
    while ( subd_times-- ) {
      mati_t new_cell; matd_t new_nods;
      green::subdivide_vox(cell, nods, new_cell, new_nods);
      cell = new_cell;
      nods = new_nods;
    }
  
    //-> pinch
    spdlog::info("pinch domain...");
    const Matrix3d F = -Matrix3d::Identity();
    std::cout << "F=" << endl << F << std::endl;
      
    matd_t deform_nods = nods;

    std::vector<double> msL;
    // visualize solution from each band
    for (int l = 1; l <= gf.num_band(); l += 2) {
      matd_t deform_nods = nods;

      clk.start();      
      for (size_t i = 0; i < nods.cols(); ++i) {
        double r, theta, phi;
        xyz_to_rpt(nods(0, i), nods(1, i), nods(2, i), r, phi, theta);      

        const auto &dG = gf.assembleGradG_l(l, r, theta, phi);
        const Vector3d &&ui = dG.topRows(3)*F.col(0)+dG.middleRows(3, 3)*F.col(1)+dG.bottomRows(3)*F.col(2);
        deform_nods.col(i) += ui;
      }
      clk.stop();
      msL.emplace_back(clk.duration()/nods.cols());

      char outf[256];
      sprintf(outf, "%s/pinch_deform-band-%03d.vtk", outdir.c_str(), l);
      hex_mesh_write_to_vtk(outf, deform_nods, cell);    
    }

    ofstream ofs(outdir+"/affine-degree-time.csv");
    ofs << "degree, time" << endl;    
    double total_ms = 0;
    for (int l = 0; l < msL.size(); ++l) {
      total_ms += msL[l];
      spdlog::info("L={}, time={}", 2*l+1, total_ms);
      ofs << 2*l+1 << "," << total_ms << endl;
    }
    ofs.close();
  }
  
  return 0;
}

static int count_over_size(ptree &pt) {
  omp_set_num_threads(NUM_THREADS);      

  std::vector<size_t> num_points{80000, 160000, 320000, 480000, 640000, 720000, 1000000};

  {
    char infile[256];
    sprintf(infile, "../data/plane-Y.obj");
    mati_t quad; matd_t verts;
    ASSERT(read_wavefront_obj(infile, quad, verts) == 0);
    spdlog::info("cell num={}, vert num={}", quad.cols(), verts.cols());
    verts *= 0.5;

    Vector3d f(0, 200, 0);
    Vector3d O = verts*vecd_t::Ones(verts.cols())/verts.cols();
    
    matd_t deform_nods = verts;
    const size_t nV = verts.cols();

    char outf[256];
    sprintf(outf, "%s/vector-N-time-%d.csv", outdir.c_str(), NUM_THREADS);

    ofstream ofs(outf);
    ofs << "N, time" << endl;

    for (const auto &N : num_points) {
      clk.start();
      #pragma omp parallel for
      for (size_t i = 0; i < N; ++i) {
        const size_t idx = i%nV;
        
        const Vector3d &u = verts.col(idx)-O;
        double r, theta, phi;
        xyz_to_rpt(u.x(), u.y(), u.z(), r, phi, theta);
        
        deform_nods.col(idx) += gf.assembleG(r, theta, phi)*f;
      }
      clk.stop();
      
      spdlog::info("N={}, time duration={} ms", N, clk.duration());
      ofs << N << "," << clk.duration() << endl;      
    }
    ofs.close();

    // char outf[256];
    // sprintf(outf, "%s/deform.obj", outdir.c_str());
    // save_wavefront_obj(outf, quad, deform_nods);
  }

  {
    // mesh
    mati_t cell(8, 1); matd_t nods(3, 8);
    {
      cell(0, 0) = 0; cell(1, 0) = 1; cell(2, 0) = 2; cell(3, 0) = 3;
      cell(4, 0) = 4; cell(5, 0) = 5; cell(6, 0) = 6; cell(7, 0) = 7;
      
      nods(0, 0) = -1;  nods(1, 0) = -1; nods(2, 0) = -1;
      nods(0, 1) = -1;  nods(1, 1) = +1; nods(2, 1) = -1;
      nods(0, 2) = +1;  nods(1, 2) = -1; nods(2, 2) = -1;
      nods(0, 3) = +1;  nods(1, 3) = +1; nods(2, 3) = -1;
      nods(0, 4) = -1;  nods(1, 4) = -1; nods(2, 4) = +1;
      nods(0, 5) = -1;  nods(1, 5) = +1; nods(2, 5) = +1;
      nods(0, 6) = +1;  nods(1, 6) = -1; nods(2, 6) = +1;
      nods(0, 7) = +1;  nods(1, 7) = +1; nods(2, 7) = +1;
    }
    int subd_times = 4;
    while ( subd_times-- ) {
      mati_t new_cell; matd_t new_nods;
      green::subdivide_vox(cell, nods, new_cell, new_nods);
      cell = new_cell;
      nods = new_nods;
    }
  
    //-> pinch
    spdlog::info("pinch domain...");
    const Matrix3d F = -Matrix3d::Identity();
      
    matd_t deform_nods = nods;
    const size_t nV = nods.cols();

    char outf[256];
    sprintf(outf, "%s/affine-N-time-%d.csv", outdir.c_str(), NUM_THREADS);
        
    ofstream ofs(outf);
    ofs << "N, time" << endl;
    
    for (const auto N : num_points) {
      clk.start();
      #pragma omp parallel for
      for (size_t i = 0; i < N; ++i) {
        const size_t idx = i%nV;

        double r, theta, phi;
        xyz_to_rpt(nods(0, idx), nods(1, idx), nods(2, idx), r, phi, theta);

        const auto &dG = gf.assembleGradG(r, theta, phi);
        const Vector3d &&ui = dG.topRows(3)*F.col(0)+dG.middleRows(3, 3)*F.col(1)+dG.bottomRows(3)*F.col(2);
        deform_nods.col(idx) += ui;
      }
      clk.stop();

      spdlog::info("N={}, duration={} ms", N, clk.duration());
      ofs << N << "," << clk.duration() << endl;
    }
    ofs.close();
      
    // char outf[256];
    // sprintf(outf, "%s/pinch_deform.vtk", outdir.c_str());
    // hex_mesh_write_to_vtk(outf, deform_nods, cell);
  }
  
  return 0;
}

static int basic_affine_load(ptree &pt) {  
  // mesh
  mati_t cell(8, 1); matd_t nods(3, 8);
  {
    cell(0, 0) = 0; cell(1, 0) = 1; cell(2, 0) = 2; cell(3, 0) = 3;
    cell(4, 0) = 4; cell(5, 0) = 5; cell(6, 0) = 6; cell(7, 0) = 7;
      
    nods(0, 0) = -1;  nods(1, 0) = -1; nods(2, 0) = -1;
    nods(0, 1) = -1;  nods(1, 1) = +1; nods(2, 1) = -1;
    nods(0, 2) = +1;  nods(1, 2) = -1; nods(2, 2) = -1;
    nods(0, 3) = +1;  nods(1, 3) = +1; nods(2, 3) = -1;
    nods(0, 4) = -1;  nods(1, 4) = -1; nods(2, 4) = +1;
    nods(0, 5) = -1;  nods(1, 5) = +1; nods(2, 5) = +1;
    nods(0, 6) = +1;  nods(1, 6) = -1; nods(2, 6) = +1;
    nods(0, 7) = +1;  nods(1, 7) = +1; nods(2, 7) = +1;
  }
  int subd_times = 4;
  while ( subd_times-- ) {
    mati_t new_cell; matd_t new_nods;
    green::subdivide_vox(cell, nods, new_cell, new_nods);
    cell = new_cell;
    nods = new_nods;
  }
  nods *= pt.get<double>("domain_scale.value");
    
  //-> affine deformtion: twist
  {
    spdlog::info("twisting domain...");
    Matrix3d F0;
    F0 <<
        0, -1, 0,
        1, 0, 0,
        0, 0, 0;

    for (size_t k = 0; k < 10; ++k) {
      const Matrix3d F = 5.0*(k+1)*F0;
      std::cout << "F=" << endl << F << std::endl;
      
      matd_t deform_nods = nods;
      #pragma omp parallel for
      for (size_t i = 0; i < nods.cols(); ++i) {
        double r, theta, phi;
        xyz_to_rpt(nods(0, i), nods(1, i), nods(2, i), r, phi, theta);

        const auto &dG = gf.assembleGradG(r, theta, phi);
        const Vector3d &&ui = dG.topRows(3)*F.col(0)+dG.middleRows(3, 3)*F.col(1)+dG.bottomRows(3)*F.col(2);
        deform_nods.col(i) += ui;
      }
      char outf[256];
      sprintf(outf, "%s/twist_deform-%03zu.vtk", outdir.c_str(), k);
      hex_mesh_write_to_vtk(outf, deform_nods, cell);
    }
  }

  //-> affine deformtion: scale
  {
    spdlog::info("scaling domain...");
    Matrix3d F0;
    F0 <<
        1, 0, 0,
        0, 1, 0,
        0, 0, 1;

    for (size_t k = 0; k < 10; ++k) {
      const Matrix3d F = -8.0*(k+1)*F0;
      std::cout << "F=" << endl << F << std::endl;      
    
      matd_t deform_nods = nods;
      #pragma omp parallel for
      for (size_t i = 0; i < nods.cols(); ++i) {
        double r, theta, phi;
        xyz_to_rpt(nods(0, i), nods(1, i), nods(2, i), r, phi, theta);

        const auto &dG = gf.assembleGradG(r, theta, phi);
        const Vector3d &&ui = dG.topRows(3)*F.col(0)+dG.middleRows(3, 3)*F.col(1)+dG.bottomRows(3)*F.col(2);
        deform_nods.col(i) += ui;
      }
      char outf[256];
      sprintf(outf, "%s/scale_deform-%03zu.vtk", outdir.c_str(), k);
      hex_mesh_write_to_vtk(outf, deform_nods, cell);
    }
  }

  //-> pinch
  {
    spdlog::info("pinch domain...");
    Matrix3d F0;
    F0 <<
        -1, 0, 0,
        0, 1, 0,
        0, 0, 0;

    for (size_t k = 0; k < 10; ++k) {
      const Matrix3d F = 1.0*(k+1)*F0;
      std::cout << "F=" << endl << F << std::endl;
      
      matd_t deform_nods = nods;
      #pragma omp parallel for
      for (size_t i = 0; i < nods.cols(); ++i) {
        double r, theta, phi;
        xyz_to_rpt(nods(0, i), nods(1, i), nods(2, i), r, phi, theta);

        const auto &dG = gf.assembleGradG(r, theta, phi);
        const Vector3d &&ui = dG.topRows(3)*F.col(0)+dG.middleRows(3, 3)*F.col(1)+dG.bottomRows(3)*F.col(2);
        deform_nods.col(i) += ui;
      }      
      char outf[256];
      sprintf(outf, "%s/pinch_deform-%03zu.vtk", outdir.c_str(), k);
      hex_mesh_write_to_vtk(outf, deform_nods, cell);
    }
  }

  //-> write configuration
  ofstream json_ofs(outdir+"/config-green.json");
  boost::property_tree::write_json(json_ofs, pt, true);
  json_ofs.close();
  
  return 0;
}

static int basic_convergence(ptree &pt) {
  char infile[256];
  sprintf(infile, "../data/plane-%c.obj", 'Y');
  mati_t quad; matd_t verts;
  ASSERT(read_wavefront_obj(infile, quad, verts) == 0);
  spdlog::info("cell num={}, vert num={}", quad.cols(), verts.cols());
  verts *= pt.get<double>("domain_scale.value");

  const double sf = 100;
  Vector3d f = Vector3d::Zero();
  f[1] = sf;
  Vector3d O = verts*vecd_t::Ones(verts.cols())/verts.cols();
  cout << "# O=" << O.transpose() << endl;

  std::vector<matd_t> UL;
  UL.emplace_back(matd_t::Zero(verts.rows(), verts.cols()));
  // for even bands
  for (int l = 0; l <= gf.num_band(); l += 2) {
    spdlog::info("l={}", l);
    matd_t u_l = matd_t::Zero(verts.rows(), verts.cols());
    #pragma omp parallel for
    for (size_t i = 0; i < verts.cols(); ++i) {
      Vector3d u = verts.col(i)-O;
      double r, theta, phi;
      xyz_to_rpt(u.x(), u.y(), u.z(), r, phi, theta);     
      u_l.col(i) = gf.assembleG_l(l, r, theta, phi)*f;
    }

    UL.emplace_back(UL.back()+u_l);

    // write obj seq
    char outf[256];
    sprintf(outf, "%s/sol-%04d.obj", outdir.c_str(), l);
    save_wavefront_obj(outf, quad, verts+UL.back());
  }

  spdlog::info("l and error: b, E");
  for (int l = 0; l < UL.size(); ++l) {
    spdlog::info("l and error: {0:2d}, {1:.8f}", 2*l, (UL[l]-UL.back()).norm()/UL.back().norm());
  }  
  
  //-> write configuration
  ofstream json_ofs(outdir+"/config-green.json");
  boost::property_tree::write_json(json_ofs, pt, true);
  json_ofs.close();
  
  return 0;
}

int main(int argc, char *argv[])
{  
  boost::property_tree::ptree pt;
  read_cmdline(argc, argv, pt);

  outdir = pt.get<string>("outdir.value");

  //  GreenElasticDeformer gf;
  ASSERT(gf.readAllG(pt.get<string>("file_allG.value").c_str()) == 0);
  ASSERT(gf.readAllGradG(pt.get<string>("file_allGradG.value").c_str()) == 0);
  spdlog::info("num of bands={}", gf.num_band());

  NUM_THREADS = pt.get<int>("num_threads.value");
  spdlog::info("NUM THREADS={}", NUM_THREADS);

  //-> REGISTER RADIAL FUNCTION
  const string reg_func = pt.get<string>("reg_func.value");
  const double reg_eps = pt.get<double>("reg_eps.value");
  spdlog::info("reg func={}", reg_func);
  spdlog::info("reg eps={}", reg_eps);
  std::shared_ptr<radial_func_t> rf;  
  if ( reg_func == "spline" ) {
    rf = make_shared<splineRadialFunc>(reg_eps, pt);
  } else if ( reg_func == "gaussian" ) {
    rf = make_shared<gsRegRadialFunc>(reg_eps);
  } else {    
    ASSERT(0);
  }
  gf.registerRadialFunc(rf);

  CALL_SUB_PROG(basic_point_load);
  CALL_SUB_PROG(basic_affine_load);
  CALL_SUB_PROG(basic_convergence);
  CALL_SUB_PROG(count_over_degree);
  CALL_SUB_PROG(count_over_size);
  
  return __LINE__;
}
