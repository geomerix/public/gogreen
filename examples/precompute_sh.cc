#include <iostream>
#include <fstream>
#include <boost/property_tree/json_parser.hpp>

#include "src/io.h"
#include "src/subdivision.h"
#include "src/ptree.h"
#include "src/green_deformer.h"
#include "src/macro.h"

using namespace std;
using namespace Eigen;
using namespace green;

static void test_integrate_sh(std::vector<double> &x, std::vector<double> &y, std::vector<double> &z, std::vector<double> &w) {
  for (int l = 0; l < 10; ++l) { // for each band up to 10
    complex_t ret(0, 0);
    for (int i = 0; i < w.size(); ++i) { // for each quadrature
      double phi, theta;
      xyz_to_tp_(&x[i], &y[i], &z[i], &phi, &theta);
      ret += w[i]*boost::math::spherical_harmonic(l, 0, theta, phi)*std::conj(boost::math::spherical_harmonic(l, 0, theta, phi));
    }
    cout << 4*M_PI*ret << endl;
  }
}
  
int main(int argc, char *argv[])
{
  boost::property_tree::ptree pt;
  read_cmdline(argc, argv, pt);

  const string outdir = pt.get<string>("outdir.value");

  const string mat_name = pt.get<string>("material.value");
  spdlog::info("material={}", mat_name);
  MatrixXd N;
  read_dense_matrix(mat_name.c_str(), N);
  ASSERT(N.rows() == 6 && N.cols() == 6);
  cout << "material tensor=" << endl << N << endl;  
  const auto &&C = convertNtoC(N);

  spdlog::info("init green fct solver for precomputation");
  const int band = pt.get<int>("num_band.value"), order = pt.get<int>("num_order.value");
  GreenElasticDeformer gf(C, band, order);
  spdlog::info("num band={}", band);
  spdlog::info("num order={}", order);

#if 1
  {
    int order = 302;
    vector<double> x(order), y(order), z(order), w(order);
    ld_by_order_(&order, &x[0], &y[0], &z[0], &w[0]);
    test_integrate_sh(x, y, z, w);
  }
#endif

#if 0
  {
    gf.debug();
    gf.debug(0.5, M_PI/6, M_PI/3);
    return __LINE__;
  }
#endif
  
#if 0
  {
    // gradient check
    const double x[3] = {0.1, 0.1, 0.1};
    const double y[3] = {0.100001, 0.1, 0.1};
    
    double rx[3], ry[3];
    xyz_to_rpt(x[0], x[1], x[2], rx[0], rx[1], rx[2]);
    xyz_to_rpt(y[0], y[1], y[2], ry[0], ry[1], ry[2]);

    const auto &Gx = gf.assembleG(rx[0], rx[2], rx[1]);
    const auto &Gy = gf.assembleG(ry[0], ry[2], ry[1]);
    cout << (Gx-Gy)/(x[0]-y[0]) << endl << endl;

    const auto &dGx = gf.assembleGradG(rx[0], rx[2], rx[1]);
    cout << dGx.topRows(3) << endl;
  }
#endif

  // write coeffcients
  gf.writeAllG(string(outdir+"/allG.mat").c_str());
  gf.writeAllGradG(string(outdir+"/allGradG.mat").c_str());  
    
  //-> write configuration
  ofstream json_ofs(outdir+"/config-precompute.json");
  boost::property_tree::write_json(json_ofs, pt, true);
  json_ofs.close();

  cout << "[INFO] done" << endl;
  return 0;
}
