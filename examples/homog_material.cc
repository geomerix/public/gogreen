#include <iostream>
#include <fstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <spdlog/spdlog.h>
#include <opencv2/opencv.hpp>

#include "src/io.h"
#include "src/subdivision.h"
#include "src/ptree.h"
#include "src/hex_fem.h"
#include "src/util.h"
#include "src/macro.h"

using namespace std;
using namespace Eigen;
using namespace green;

#define SQ(x) ((x)*(x))

class mtr_generator
{
 public:
  mtr_generator(const mati_t &cube, const matd_t &nods, const boost::property_tree::ptree &pt)
      : cube_(cube), nods_(nods), pt_(pt) {
    const double max_E = pt.get<double>("max_E.value");
    const double min_E = pt.get<double>("min_E.value");
    const double nu    = pt.get<double>("poisson_ratio.value");
    spdlog::info("maxE={}, minE={}, nu={}", max_E, min_E, nu);

    dx_ = (nods.col(cube_(0, 0))-nods.col(cube_(1, 0))).norm();
    dy_ = (nods.col(cube_(0, 0))-nods.col(cube_(2, 0))).norm();
    dz_ = (nods.col(cube_(0, 0))-nods.col(cube_(4, 0))).norm();
    spdlog::info("dx={}, dy={}, dz={}", dx_, dy_, dz_);
    
    max_lame_.resize(2); min_lame_.resize(2);
    calc_lame_const(min_E, nu, min_lame_[0], min_lame_[1]);
    calc_lame_const(max_E, nu, max_lame_[0], max_lame_[1]);
    cout << "# min_lame=" << min_lame_.transpose() << endl;
    cout << "# max_lame=" << max_lame_.transpose() << endl;
  }
  matd_t generate() const {
    const string option = pt_.get<string>("mtr_name.value");
    spdlog::info("material name={}", option);
    
    matd_t mtr = matd_t::Zero(2, cube_.cols());
    if ( option == "HOMO" ) {
      this->HOMO(mtr);
    } else if ( option == "BONE" ) {
      this->BONE(mtr);
    } else if ( option == "SLOP" ) {
      this->SLOP(mtr);
    } else if ( option == "ABBA" ) {
      this->ABBA(mtr);
    } else if ( option == "WAVE" ) {
      this->WAVE(mtr);
    } else if ( option == "CIRC" ) {
      this->CIRC(mtr);
    } else if ( option == "IMAG" ) {
      const string &img_file = pt_.get<string>("img_file.value");
      this->IMAG(mtr, img_file);
    } else if ( option.substr(0, 4) == "SINE" ) {
      this->SINE(mtr, std::stoi(option.substr(4)));
    } else if ( option.substr(0, 4) == "RAND" ) {
      this->RAND(mtr, std::stoi(option.substr(4)));
    } else {
      ASSERT(0);
    }

    return mtr;
  }  

 private:
  void SLOP(matd_t &mtr) const {
    const double d = sqrt(dx_*dx_+dy_*dy_+dz_*dz_);    
    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();
      
      //      const double f = fabs(c.x()+c.y()+c.z()+3);
      const double f = fabs(c.x()+c.z()+2);
      if ( static_cast<int>(std::floor(f/d))%4 == 0 ) {
        // static_cast<int>(std::floor(f/d))%4 == 3 ) {
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }
    }
  }
  void SINE(matd_t &mtr, const int T) const {
    spdlog::info("T={}", T);
    const double w = 2*M_PI*T;
    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();
      
      const double f = sin(w*c.x())*cos(w*c.y())+sin(w*c.z())*cos(w*c.x())+sin(w*c.y())*cos(w*c.z());
      if ( f < 0 ) {
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }
    }
  }
  void BONE(matd_t &mtr) const {
    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();

      double TL = 2*dx_*dx_;
      if ( SQ(c.x()-0.0)+SQ(c.z()-0.0) < TL ||
           SQ(c.x()-0.5)+SQ(c.z()-0.5) < TL ||
           SQ(c.x()-0.5)+SQ(c.z()+0.5) < TL ||
           SQ(c.x()+0.5)+SQ(c.z()-0.5) < TL ||
           SQ(c.x()+0.5)+SQ(c.z()+0.5) < TL ) {
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }
    }
  }
  void WAVE(matd_t &mtr) const {
    const size_t nods_num = nods_.cols();
    const vecd_t center = nods_*vecd_t::Ones(nods_num)/nods_num;

    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();

      Vector3d d = c-center;
      int dist = floor((fabs(d.x())+fabs(d.y()+fabs(d.z())))/dx_+0.5);
      if ( (dist/2)%2 == 0 ) {
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }      
    }
  }
  void RAND(matd_t &mtr, const int mod) const {
    for (size_t i = 0; i < cube_.cols(); ++i) {
      if ( rand()%mod == 0 ) {
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }
    }
  }
  void ABBA(matd_t &mtr) const {
    const double min_x = nods_.row(0).minCoeff();
    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();      
      const double xc = c[0];
      const size_t idx = (xc-min_x)/dx_;
      if ( idx % 4 == 0 || idx % 4 == 3 ) {       
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }
    }
  }
  void CIRC(matd_t &mtr) const {
    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();      
      const double xc = c[0];
      const double zc = c[2];
      const size_t idx = std::sqrt(xc*xc+zc*zc)/dx_;
      if ( idx % 2 == 0 ) {  
        mtr.col(i) = max_lame_;
      } else {
        mtr.col(i) = min_lame_;
      }
    }
  }  
  void HOMO(matd_t &mtr) const {
    mtr = (max_lame_+min_lame_)/2*MatrixXd::Ones(1, mtr.cols());
  }
  void IMAG(matd_t &mtr, const std::string &img_file) const {
    cv::Mat color_img, gray_img;
    color_img = cv::imread(img_file, cv::IMREAD_COLOR);

    const int n = std::cbrt(cube_.cols());
    spdlog::info("img res={}x{}", color_img.rows, color_img.cols);
    spdlog::info("n={}", n);
    cv::resize(color_img, color_img, cv::Size(n, n), 0, 0, cv::INTER_LINEAR); // make it square
    cv::cvtColor(color_img, gray_img, cv::COLOR_BGR2GRAY);                    // gray image
    cv::threshold(gray_img, gray_img, 220, 255, cv::THRESH_BINARY);

    string out_file = pt_.get<string>("outdir.value")+"/rescale.png";
    cv::imwrite(out_file, color_img);
    out_file = pt_.get<string>("outdir.value")+"/gray.png";
    cv::imwrite(out_file, gray_img);

    double min_x = nods_.row(0).minCoeff(), max_x = nods_.row(0).maxCoeff();
    double min_y = nods_.row(1).minCoeff(), max_y = nods_.row(1).maxCoeff();
    double min_z = nods_.row(2).minCoeff(), max_z = nods_.row(2).maxCoeff();
    double dx = (max_x-min_x)/n, dy = (max_y-min_y)/n, dz = (max_z-min_z)/n;
    spdlog::info("domain: [{}, {}]x[{}, {}]x[{}, {}]", min_x, max_x, min_y, max_y, min_z, max_z);

    for (size_t i = 0; i < cube_.cols(); ++i) {
      Vector3d c = Vector3d::Zero();
      for (size_t j = 0; j < cube_.rows(); ++j) {
        c += nods_.col(cube_(j, i));
      }
      c /= cube_.rows();
      
      int q = std::max(std::min(static_cast<int>(fabs(c[0]-min_x)/dx), n-1), 0);
      int p = std::max(std::min(static_cast<int>(fabs(c[2]-min_z)/dz), n-1), 0);
      int r = std::max(std::min(static_cast<int>(fabs(c[1]-min_y)/dy), n-1), 0);
      mtr.col(i) = gray_img.at<uchar>(p, q) == 255 ? min_lame_ : max_lame_;
    }
  }

 private:
  const mati_t &cube_;
  const matd_t &nods_;
  const boost::property_tree::ptree &pt_;

  double dx_, dy_, dz_;
  vecd_t max_lame_, min_lame_;
};

int main(int argc, char *argv[])
{
  boost::property_tree::ptree pt;
  read_cmdline(argc, argv, pt);

  const string outdir = pt.get<string>("outdir.value");

  const string MTR_NAME = pt.get<string>("mtr_name.value");
  Matrix6d C;
  if ( MTR_NAME == "ORTH" ) {
    // direct manipulation
    Matrix6d S = Matrix6d::Zero();
    double Ex = pt.get<double>("Ex.value", 1000);
    double Ey = pt.get<double>("Ey.value", 1000);
    double Ez = pt.get<double>("Ez.value", 1000);
    
    // double Vxy = pt.get<double>("Vxy.value", -0.9);
    // double Vxz = pt.get<double>("Vxz.value", 0.45);

    // double Vxy = pt.get<double>("Vxy.value", 0.49);
    // double Vxz = pt.get<double>("Vxz.value", 0.49);

    double Vxy = pt.get<double>("Vxy.value", -0.65);
    double Vxz = pt.get<double>("Vxz.value", -0.65);
        
    double Vyz = pt.get<double>("Vyz.value", 0);
    double Vyx = Ey*Vxy/Ex;
    double Vzx = Ez*Vxz/Ex;
    double Vzy = Ez*Vyz/Ey;
    double Gyz = pt.get<double>("Gyz.value", 500);
    double Gzx = pt.get<double>("Gzx.value", 500);
    double Gxy = pt.get<double>("Gxy.value", 500);    

    S(0, 0) = 1.0/Ex;
    S(0, 1) = -Vyx/Ey;
    S(0, 2) = -Vzx/Ez;
    S(1, 0) = -Vxy/Ex;
    S(1, 1) = 1.0/Ey;
    S(1, 2) = -Vzy/Ez;
    S(2, 0) = -Vxz/Ex;
    S(2, 1) = -Vyz/Ey;
    S(2, 2) = 1.0/Ez;
    S(3, 3) = 1.0/Gyz;
    S(4, 4) = 1.0/Gzx;
    S(5, 5) = 1.0/Gxy;
    
    C = S.inverse();
  } else {
    // homogenization
    mati_t cell(8, 1); matd_t nods(3, 8);
    {
      cell(0, 0) = 0; cell(1, 0) = 1; cell(2, 0) = 2; cell(3, 0) = 3;
      cell(4, 0) = 4; cell(5, 0) = 5; cell(6, 0) = 6; cell(7, 0) = 7;
      
      nods(0, 0) = -1;  nods(1, 0) = -1; nods(2, 0) = -1;
      nods(0, 1) = -1;  nods(1, 1) = +1; nods(2, 1) = -1;
      nods(0, 2) = +1;  nods(1, 2) = -1; nods(2, 2) = -1;
      nods(0, 3) = +1;  nods(1, 3) = +1; nods(2, 3) = -1;
      nods(0, 4) = -1;  nods(1, 4) = -1; nods(2, 4) = +1;
      nods(0, 5) = -1;  nods(1, 5) = +1; nods(2, 5) = +1;
      nods(0, 6) = +1;  nods(1, 6) = -1; nods(2, 6) = +1;
      nods(0, 7) = +1;  nods(1, 7) = +1; nods(2, 7) = +1;
    }

    int subd_times = 4;
    while ( subd_times-- ) {
      mati_t new_cell; matd_t new_nods;
      green::subdivide_vox(cell, nods, new_cell, new_nods);
      cell = new_cell;
      nods = new_nods;
    }

    // generate material
    mtr_generator mtg(cell, nods, pt);
    const matd_t &lame = mtg.generate();
    const vecd_t &color = lame.row(0);  
    {
      const string &file = outdir+"/grid.vtk";
      hex_mesh_write_to_vtk(file.c_str(), nods, cell, &color, "CELL");
    }

    TwoLevHexFEM fem(cell, nods, lame);
    matd_t hab;
    fem.globalHarmonics(hab);
    {
      const double scale_gh = pt.get<double>("scale_gh.value", 1.0);
      for (int j = 0; j < hab.cols(); ++j) {
        char outf[256];
        sprintf(outf, "%s/gh-%d.vtk", outdir.c_str(), j);

        const MatrixXd &pos = nods+scale_gh*hab.col(j).reshaped(nods.rows(), nods.cols());
        hex_mesh_write_to_vtk(outf, pos, cell, &color, "CELL");
      }
    }

    C = fem.homogC(hab);
  }

  write_dense_matrix(string(outdir+"/C.mat").c_str(), C);
  spdlog::info("C size={}x{}", C.rows(), C.cols());
  cout << C << endl;

  // compute the compliance tensor
  Matrix6d S = C.inverse();
  S.block<3, 3>(0, 3) *= 0.5;
  S.block<3, 3>(3, 0) *= 0.5;
  S.block<3, 3>(3, 3) *= 0.25;
  const Vector3d X = Vector3d::UnitX(); // direction of extension
  const Vector3d Z = Vector3d::UnitZ(), Y = Vector3d::UnitY();
  double E_n = 0;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      for (int k = 0; k < 3; ++k) {
        for (int l = 0; l < 3; ++l) {
          double Sijkl = S(voigt_map(i, j), voigt_map(k, l));
          E_n += Sijkl*X[i]*X[j]*X[k]*X[l];
        }
      }
    }
  }
  E_n = 1.0/E_n;
  spdlog::info("En={}", E_n);
  double nuZ = 0.0, nuY = 0.0;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      for (int k = 0; k < 3; ++k) {
        for (int l = 0; l < 3; ++l) {
          double Sijkl = S(voigt_map(i, j), voigt_map(k, l));
          nuZ += -E_n*Sijkl*X[i]*X[j]*Z[k]*Z[l];
          nuY += -E_n*Sijkl*X[i]*X[j]*Y[k]*Y[l];
        }
      }
    }
  }
  spdlog::info("nuZ={}, nuY={}", nuZ, nuY);
  
  //-> write configuration
  ofstream json_ofs(outdir+"/config.json");
  boost::property_tree::write_json(json_ofs, pt, true);
  json_ofs.close();

  cout << "[INFO] done" << endl;
  return 0;
}
