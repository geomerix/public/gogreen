#include <iostream>
#include <fstream>
#include <boost/property_tree/json_parser.hpp>
#include <igl/readOBJ.h>
#include <igl/writeOBJ.h>

#include "src/io.h"
#include "src/subdivision.h"
#include "src/ptree.h"
#include "src/green_deformer.h"
#include "src/macro.h"

using namespace std;
using namespace Eigen;
using namespace green;

int main(int argc, char *argv[])
{
  boost::property_tree::ptree pt;
  read_cmdline(argc, argv, pt);

  const string outdir = pt.get<string>("outdir.value");

  GreenElasticDeformer gf;
  ASSERT(gf.readAllG(pt.get<string>("file_allG.value").c_str()) == 0);
  ASSERT(gf.readAllGradG(pt.get<string>("file_allGradG.value").c_str()) == 0);
  const int band = gf.num_band();
  spdlog::info("num of bands={}", band);

  //-> REGISTER RADIAL FUNCTION
  const string reg_func = pt.get<string>("reg_func.value");
  const double reg_eps = pt.get<double>("reg_eps.value");
  std::shared_ptr<radial_func_t> rf;
  if ( reg_func == "spline" ) {
    rf = make_shared<splineRadialFunc>(reg_eps, pt);
  } else if ( reg_func == "gaussian" ) {
    rf = make_shared<gsRegRadialFunc>(reg_eps);
  } else {
    ASSERT(0);
  }
  gf.registerRadialFunc(rf);
  spdlog::info("reg eps={}", reg_eps);

  const string mesh_file = pt.get<string>("ref_mesh.value");
  matd_t V, TC, CN; mati_t F, FTC, FN;
  ASSERT(igl::readOBJ(mesh_file, V, TC, CN, F, FTC, FN) == true);
  spdlog::info("V size={}x{}", V.rows(), V.cols());
  spdlog::info("TC size={}x{}", TC.rows(), TC.cols());
  spdlog::info("F size={}x{}", F.rows(), F.cols());

  const string edit_file = pt.get<string>("edt_mesh.value");
  matd_t edit_V; mati_t edit_F;
  ASSERT(igl::readOBJ(edit_file, edit_V, edit_F) == true);
  ASSERT(V.rows() == edit_V.rows() && F.rows() == edit_F.rows());
  ASSERT((F-edit_F).norm() == 0);
  {
    igl::writeOBJ(string(outdir+"/ref_mesh.obj"), V, F, CN, FN, TC, FTC);
    igl::writeOBJ(string(outdir+"/editing.obj"), edit_V, F, CN, FN, TC, FTC);
  }

  std::vector<double> origin, force;
  for (size_t i = 0; i < V.rows(); ++i) {
    Vector3d d = edit_V.row(i)-V.row(i);
    if ( d.squaredNorm() > 1e-4 ) {
      origin.emplace_back(V(i, 0));
      origin.emplace_back(V(i, 1));
      origin.emplace_back(V(i, 2));
      force.emplace_back(d.x());
      force.emplace_back(d.y());
      force.emplace_back(d.z());
    }
  }
  const size_t num_green = origin.size()/3;
  spdlog::info("num of greenlets={}", num_green);

  const double force_scale = pt.get<double>("force_scale.value");
  spdlog::info("force scale={}", force_scale);
  
  // deform by greenlets
  spdlog::info("V norm={}", V.norm());
  matd_t deformV = V;
  #pragma omp parallel for
  for (size_t i = 0; i < V.rows(); ++i) {
    for (size_t j = 0; j < num_green; ++j) {
      Vector3d u(V(i, 0)-origin[3*j+0], V(i, 1)-origin[3*j+1], V(i, 2)-origin[3*j+2]);
      double r, theta, phi;
      xyz_to_rpt(u.x(), u.y(), u.z(), r, phi, theta);     
      deformV.row(i) += gf.assembleG(r, theta, phi)*force_scale*Vector3d(&force[3*j]);
    }
  }
  spdlog::info("deformV norm={}", deformV.norm());

  char outf[256];
  sprintf(outf, "%s/deform_mesh-%d.obj", outdir.c_str(), (int)force_scale);
  igl::writeOBJ(string(outf), deformV, F, CN, FN, TC, FTC);
  
  //-> write configuration
  ofstream json_ofs(outdir+"/config-demo.json");
  boost::property_tree::write_json(json_ofs, pt, true);
  json_ofs.close();

  cout << "[INFO] done" << endl;
  return 0;
}
