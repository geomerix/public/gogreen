#include <iostream>
#include <fstream>
#include <vector>

#include "src/util.h"
#include "src/radial_func.h"

using namespace std;
using namespace Eigen;
using namespace green;

int main(int argc, char *argv[])
{
#if 0
  const double eps = 0.02;
  const int P = 0;

  VectorXd r = VectorXd::LinSpaced(1000, 1e-6, 1.0);
  cout << r.size() << endl;
  for (int l = 0; l <= 6; ++l) {
    char file[128];
    sprintf(file, "GS-%lf-band-%d.csv", eps, l);

    ofstream ofs(file);
    ofs << "x, f" << endl;
    for (size_t j = 0; j < r.size(); ++j) {
      ofs << r[j] << "," << gsRegRadialFunc(l, r[j], eps, P) << endl;
    }
    ofs.close();
  }

  return 0;
#endif
  
  int order = atoi(argv[1]);
  std::vector<double> x(order), y(order), z(order), w(order);
  green::ld_by_order_(&order, &x[0], &y[0], &z[0], &w[0]);  
  for (int i = 0; i < order; ++i) {
    cout << x[i] << ", " << y[i] << ", " << z[i] << endl;
  }
  return 0;
}
