#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

for mtr_name in ABBA; do
max_e=10
min_e=10

reg_func=spline
reg_eps=0.2

num_band=8

ref_mesh=../data/spot-subd.obj
edt_mesh=../data/spot-edit-3.obj

basedir=../result/green/$(date -I)/
make -f pipeline.mk cons BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} BAND0_XY=spot-band-0.xy BAND2_XY=spot-band-2.xy
done 
