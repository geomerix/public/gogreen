import numpy as np
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import os, sys
from edit_curve import interpolate, PointR, AffineR

if __name__ == "__main__":
    eps = 1.6
    
    upper_x = 10.0
    n = 50
    dx = upper_x/(n-1)
    
    L = int(sys.argv[1])
    A = float(sys.argv[2])
    W = float(sys.argv[3])
    print("L=", L, "A=", A, "W=", W)

    fig = plt.figure(figsize=(6,3.5), dpi=200)

    # for odd degree
    for l in range(1, 7, 2):
        print("l=", l)    
        RadialF = AffineR

        a = A if l == L else 1.0
        w = W if l == L else 1.0
        print("a=", a, "w=", w)

        if l == 1:
            ax = fig.add_subplot(311)
        elif l == 3:
            ax = fig.add_subplot(312)
        else:
            ax = fig.add_subplot(313)
            
        verts = []
        max_x, max_y = 0.0, 0.0
        for i in range(n):
            val_x = w*i*dx
            val_y = a*RadialF(val_x, l, eps)
            max_y = max(max_y, val_y)
            max_x = max(max_x, val_x)
            verts.append((val_x, val_y))

        x, y = interpolate(*zip(*verts))
        spline = Line2D(x,y, linewidth=4)
        ax.add_line(spline)

        if l%2 == 0:
            ax.set_title(r'$R_{}(r)$'.format(l))
        else:
            ax.set_title(r'\mathcal R_{}(r)'.format(l))

        ax.set_xlim(0, upper_x)
        ax.set_ylim(0, 1.1*max_y)

        outfile = 'band-{}.xy'.format(l)
        np.savetxt(outfile, verts, delimiter=' ')

    #plt.tight_layout()
    #plt.show()
