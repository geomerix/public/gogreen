#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

mtr_name=ABBA
max_e=50
min_e=50
reg_func=gaussian
reg_eps=0.2
domain_scale=1

num_band=8

basedir=../result/green/$(date -I)/
make -f pipeline.mk green PROG=basic_affine_load BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DOMAIN_SCALE=${domain_scale} NUM_ORDER=50
