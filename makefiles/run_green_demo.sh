#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

mtr_name=ABBA
max_e=100
min_e=100

reg_func=gaussian
reg_eps=0.2

num_band=8

ref_mesh=../data/spot/spot_quadrangulated.obj
edt_mesh=../data/spot/spot_quadrangulated_edit.obj
force_scale=100

basedir=../result/green/$(date -I)/
make -f pipeline.mk demo BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DEMO_REF_MESH=${ref_mesh} DEMO_EDT_MESH=${edt_mesh} FORCE_SCALE=${force_scale}
