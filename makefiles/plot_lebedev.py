import numpy as np
import matplotlib.pyplot as plt
import os

order = [6, 14, 26, 38, 50]
color = ['red', 'darkorange', 'teal', 'deepskyblue', 'blue']
    
for i in range(len(order)):
    p = order[i]
    c = color[i]
    
    fig = plt.figure()
    os.system('../build/examples/get_lebedev {} > leb.txt'.format(p))

    X = np.genfromtxt('leb.txt', dtype=np.float64, delimiter=',')
    print(X.shape)

    ax = fig.add_subplot(111, projection='3d')

    R = 1.0
    u, v = np.mgrid[0:2 * np.pi:17j, 0:np.pi:9j]
    x = R*np.cos(u) * np.sin(v)
    y = R*np.sin(u) * np.sin(v)
    z = R*np.cos(v)

    ax.view_init(elev=0, azim=-45)
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], s=80, c=c)
    ax.plot_wireframe(x, y, z, color='black', linewidth=1.5, alpha=0.8)
    ax.set_axis_off()

    plt.savefig('lebedev-{}.png'.format(p))

os.system('rm leb.txt')
