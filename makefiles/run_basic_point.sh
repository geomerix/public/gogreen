#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

mtr_name=ABBA
max_e=75
min_e=0.75

reg_func=gaussian
reg_eps=0.2
domain_scale=0.5

num_band=100
num_order=5810

basedir=../result/green/$(date -I)-DECOMPOSE/
make -f pipeline.mk green PROG=basic_point_load BASEDIR=${basedir} MTR_NAME=${mtr_name} MAX_E=${max_e} MIN_E=${min_e} NUM_BAND=${num_band} REG_FUNC=${reg_func} REG_EPS=${reg_eps} DOMAIN_SCALE=${domain_scale} NUM_ORDER=${num_order}
