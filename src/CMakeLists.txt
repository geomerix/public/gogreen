enable_language(Fortran)

file(GLOB source *.cc *.c *.f90)
add_library(green SHARED ${source})
