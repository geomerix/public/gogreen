#include "hex_fem.h"
#include "util.h"
#include "macro.h"
#include "subdivision.h"

#include <spdlog/spdlog.h>

using namespace Eigen;
using namespace std;

namespace green {

extern "C" {
  void vox_linear_hes_(double *hes, const double *HDmH, const double *det,
                       const double *mu, const double *lambda);
}

struct moment_constraint
{
  typedef Eigen::Triplet<double> TPL;

  moment_constraint(const matd_t &nods) : nods_(nods), dim_(nods.size()) {
    const size_t n = nods.cols();
    xbar_ = nods_*VectorXd::Ones(n)/n;
    cout << "xbar=" << xbar_.transpose() << endl;
  }
  size_t dim_x() const {
    return dim_;
  }
  size_t dim_f() const {
    return 6;
  }
  int Val(const double *x, double *val) const {
    Map<const MatrixXd> X(x, 3, dim_x()/3);
    Map<VectorXd> F(val, dim_f());    
    for (size_t i = 0; i < nods_.cols(); ++i) {
      F.head(3) += X.col(i)-nods_.col(i);
      Vector3d xi = X.col(i)-nods_.col(i), xb = X.col(i)-xbar_;
      F.tail(3) += xi.cross(xb);
    }
    return 0;
  }
  int Jac(const double *x, const size_t off, std::vector<TPL> &jac) const {
    for (size_t i = 0; i < nods_.cols(); ++i) {
      jac.emplace_back(TPL(off+0, 3*i+0, 1));
      jac.emplace_back(TPL(off+1, 3*i+1, 1));
      jac.emplace_back(TPL(off+2, 3*i+2, 1));

      const double j01 = nods_(2, i)-xbar_[2];
      jac.emplace_back(TPL(off+3, 3*i+1, j01));
      jac.emplace_back(TPL(off+4, 3*i+0, -j01));

      const double j02 = xbar_[1]-nods_(1, i);
      jac.emplace_back(TPL(off+3, 3*i+2, j02));
      jac.emplace_back(TPL(off+5, 3*i+0, -j02));

      const double j12 = nods_(0, i)-xbar_[0];
      jac.emplace_back(TPL(off+4, 3*i+2, j12));
      jac.emplace_back(TPL(off+5, 3*i+1, -j12));
    }
    return 0;
  }
  
  const size_t dim_;
  const matd_t nods_;
  vecd_t xbar_;
};

TwoLevHexFEM::TwoLevHexFEM(const mati_t &cell, const matd_t &nods, const matd_t &lame)
    : cell_h_(cell), nods_h_(nods), lame_h_(lame) {
  ASSERT(cell.rows() == 8 && nods.rows() == 3 && lame.rows() == 2 && cell.cols() == lame.cols());
  
  const double QV[2] = {-1.0/sqrt(3.0), +1.0/sqrt(3.0)};
  const double QW[2] = {1.0, 1.0};
  const size_t QN = 2*2*2;
  qp_ = Eigen::Matrix3Xd::Zero(3, QN);
  qw_.resize(QN);
  for (size_t i = 0; i < 2; ++i) {
    for (size_t j = 0; j < 2; ++j) {
      for (size_t k = 0; k < 2; ++k) {
        const size_t idx = 4*i+2*j+k;
        qp_(0, idx) = QV[i];
        qp_(1, idx) = QV[j];
        qp_(2, idx) = QV[k];
        qw_[idx] = QW[i]*QW[j]*QW[k];
      }
    }
  }
  
  detDmH_.resize(QN*cell.cols());
  HDmH_.resize(24, QN*cell.cols());
  {
    #pragma omp parallel for
    for (size_t i = 0; i < cell.cols(); ++i) {
      const Matrix3x8d &X = nods(Eigen::all, cell.col(i));
      
      for (size_t j = 0; j < QN; ++j) { // for eight quadratures
        Matrix8x3d H = Matrix8x3d::Zero();
        vox_SF_jac(H.data(), &qp_(0, j));

        const Matrix3d &DmH = X*H;
        const size_t idx = QN*i+j;
        detDmH_[idx] = fabs(DmH.determinant());
        HDmH_.col(idx).reshaped(8, 3) = H*DmH.inverse();
      }
    } 
  }

  {
    // coarse part
    const Matrix3x8d &X_H = nods.leftCols(8);

    const double qp[3] = {0};    
    Matrix8x3d H = Matrix8x3d::Zero();
    vox_SF_jac(H.data(), qp);

    const Matrix3d &DmH = X_H*H;
    detDmH_H_ = fabs(DmH.determinant());
    HDmH_H_ = (H*DmH.inverse()).reshaped();
    spdlog::info("detDmH_H={}", detDmH_H_);
  }
}

void TwoLevHexFEM::assembleK(std::vector<Eigen::Triplet<double>> &hes) const {
  #pragma omp parallel for
  for (size_t i = 0; i < cell_h_.cols(); ++i) {
    Matrix24d H = Matrix24d::Zero(24, 24);
    for (size_t j = 0; j < 8; ++j) {
      const size_t idx = 8*i+j;
      Matrix24d Hr = Matrix24d::Zero();
      vox_linear_hes_(Hr.data(), &HDmH_(0, idx), &detDmH_[idx], &lame_h_(0, i), &lame_h_(1, i));
      H += Hr;
    }

    std::vector<Eigen::Triplet<double>> loc_trips;
    for (size_t p = 0; p < H.rows(); ++p) {
      for (size_t q = 0; q < H.cols(); ++q) {
        const size_t I = 3*cell_h_(p/3, i)+p%3;
        const size_t J = 3*cell_h_(q/3, i)+q%3;
        if ( fabs(H(p, q)) > 0.0 ) {
          loc_trips.emplace_back(Eigen::Triplet<double>(I, J, H(p, q)));
        }        
      }
    }

    #pragma omp critical
    {
      hes.insert(hes.end(), loc_trips.begin(), loc_trips.end());
    }
  }
}

void TwoLevHexFEM::globalHarmonics(matd_t &hab) const {
  // get boundary surface
  vox_faces vf(cell_h_);
  std::vector<size_t> bd_facet;
  for (const auto &f : vf.face2vox_) {
    if ( f.second.size() == 1 ) {
      auto &facet = vf.face_in_order_[f.first];
      bd_facet.insert(bd_facet.end(), facet.begin(), facet.end());
    }
  }
  const size_t num_bd_facet = bd_facet.size()/4;
  spdlog::info("boundary facets num={}", num_bd_facet);

  // compute normal
  Eigen::Matrix3Xd face_n = Eigen::Matrix3Xd::Zero(3, num_bd_facet);
  for (size_t i = 0; i < face_n.cols(); ++i) {
    face_n.col(i) = (nods_h_.col(bd_facet[4*i+1])-nods_h_.col(bd_facet[4*i]))
        .cross(nods_h_.col(bd_facet[4*i+2])-nods_h_.col(bd_facet[4*i])).normalized();
  }

  moment_constraint constraint(nods_h_);
  const size_t N = constraint.dim_x();
  ASSERT(N == nods_h_.size());
  const size_t prb_size = constraint.dim_x()+constraint.dim_f();
  
  Eigen::SparseMatrix<double> LHS(prb_size, prb_size);
  {
    vector<Triplet<double>> trips;
    assembleK(trips);
    constraint.Jac(nullptr, N, trips);
    LHS.setFromTriplets(trips.begin(), trips.end());
  }
  spdlog::info("LHS size={}", LHS.rows());  
  spdlog::info("prefactorize LHS");
  Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>, Eigen::Lower> ldlt;
  ldlt.compute(LHS);
  ASSERT(ldlt.info() == Eigen::Success);

  const Matrix3d Id = Matrix3d::Identity();
  hab.resize(N, 6);
  int cnt = 0;
  for (int i = 0; i < 3; ++i) {
    for (int j = i; j < 3; ++j) {
      spdlog::info("process {} {}", i, j);
      const Matrix3d &P = (Id.col(j)*Id.col(i).transpose()+Id.col(i)*Id.col(j).transpose())/2;

      VectorXd g = VectorXd::Zero(prb_size);
      for (int k = 0; k < num_bd_facet; ++k) {
        const Vector3d &v = P*face_n.col(k)/4;
        g.segment<3>(3*bd_facet[4*k+0]) += v;
        g.segment<3>(3*bd_facet[4*k+1]) += v;
        g.segment<3>(3*bd_facet[4*k+2]) += v;
        g.segment<3>(3*bd_facet[4*k+3]) += v;
      }
      constraint.Val(nods_h_.data(), g.data()+N);
      g *= -1;

      hab.col(cnt++) = ldlt.solve(g).head(N);
      ASSERT(ldlt.info() == Eigen::Success);
    }
  }
}

Matrix6d TwoLevHexFEM::homogC(const matd_t &hab) const {
  ASSERT(hab.cols() == 6);
  const size_t N = nods_h_.size();
  
  vector<Triplet<double>> trips;
  assembleK(trips);
  SparseMatrix<double> K(N, N);
  K.setFromTriplets(trips.begin(), trips.end());

  matd_t LHS = matd_t::Zero(36, 36);
  vecd_t rhs = vecd_t::Zero(36); 

  //-> for each pair of (h_ab, h_cd)
  for (size_t i = 0; i < 6; ++i) {
    for (size_t j = 0; j < 6; ++j) {
      const size_t IDX = 6*i+j;

      //-> downsample harmonic displacements on coarse mesh
      const matd_t &Gi = hab.col(i).head(3*8).reshaped(3, 8);
      const matd_t &Gj = hab.col(j).head(3*8).reshaped(3, 8);

      //-> get LHS, integrate over coarse cell
      const auto &Si = calc_cauchy_strain(Gi, HDmH_H_);
      const auto &Sj = calc_cauchy_strain(Gj, HDmH_H_);
      const matd_t &Dc = 8.0*detDmH_H_*Si*Sj.transpose();
      ASSERT(Dc.rows() == 6 && Dc.cols() == 6);
      
      LHS.row(IDX) = Dc.reshaped().transpose();
      rhs[IDX] = hab.col(i).dot(K*hab.col(j));
    }
  }

  cout << "check difference" << endl;
  cout << (LHS.transpose()-LHS).norm() << endl;

  Matrix6d C = Matrix6d::Zero();
  C.reshaped() = LHS.lu().solve(rhs);
  return C;
}

}
