#ifndef RADIAL_FUNC_H
#define RADIAL_FUNC_H

#define USE_GSL

#ifdef USE_GSL
  #include <gsl/gsl_sf_gamma.h>
  #include <gsl/gsl_sf_hyperg.h>
#else
  #include <boost/math/special_functions/gamma.hpp>
  #include <boost/math/special_functions/hypergeometric_1F1.hpp>
#endif

#include <boost/property_tree/ptree.hpp>
#include <spdlog/spdlog.h>
#include "util.h"
#include "spline.h"
#include "io.h"
#include "macro.h"

namespace green {

enum load_type {VECTOR, AFFINE};

struct radial_func_t
{
  virtual double operator()(const int l, const double r, const load_type order) const = 0;
};

inline double GAMMA(const double x) {
#ifdef USE_GSL
  return gsl_sf_gamma(x);
#else
  return boost::math::tgamma(x);
#endif
}

inline double REG_HYPERGF11(const double a, const double b, const double x) {
#ifdef USE_GSL
  return gsl_sf_hyperg_1F1(a, b, x)/gsl_sf_gamma(b);
#else
  return boost::math::hypergeometric_1F1_regularized(a, b, x);
#endif    
}

struct singularRadialFunc final : radial_func_t
{
  double operator()(const int l, const double r, const load_type order) const override {
    if ( order == VECTOR ) {
      return std::sqrt(M_PI)*GAMMA((l+1)/2.0)/(2*r*GAMMA((l+2)/2.0));
    }
    if ( order == AFFINE ) {
      return std::sqrt(M_PI)*GAMMA((l+2)/2.0)/(r*r*GAMMA((l+1)/2.0));
    }
    return std::sqrt(-1.0);    
  }
};

struct gsRegRadialFunc final : radial_func_t
{
  double eps_;
  const double SQRT_PI = std::sqrt(M_PI);
  const int MAXL = 150;  
  Eigen::VectorXd greenL_, gammaL_;

  gsRegRadialFunc(const double eps) : eps_(eps) {
    greenL_ = gammaL_ = Eigen::VectorXd::Zero(MAXL);
    for (int l = 0; l < greenL_.size(); ++l) {
      greenL_[l] = l%2 == 0 ? std::pow(eps,-1 - l)*GAMMA((1 + l)/2.) : std::pow(eps,-2 - l)*GAMMA(1 + l/2.);
      gammaL_[l] = 1.0/GAMMA(1.5+l);
    }
  }
  double operator()(const int l, const double r, const load_type order) const override {
    const double eps = eps_;
    if ( order == VECTOR ) {
      return (SQRT_PI*std::pow(r,l)*greenL_[l]*gsl_sf_hyperg_1F1((1 + l)/2.,1.5 + l,-(r*r)/(eps*eps))*gammaL_[l])/2.;
    }

    if ( order == AFFINE ) {
      return SQRT_PI*std::pow(r,l)*greenL_[l]*gsl_sf_hyperg_1F1(1 + l/2.,1.5 + l,-(r*r)/(eps*eps))*gammaL_[l];
    }

    return std::sqrt(-1.0);
  }
};

struct splineRadialFunc final : radial_func_t
{
  double eps_;
  const double SQRT_PI = std::sqrt(M_PI);
  const int MAXL = 150;  
  Eigen::VectorXd greenL_, gammaL_;  
  std::shared_ptr<tk::spline> s0_, s2_, s4_;
  std::shared_ptr<tk::spline> s1_, s3_, s5_;
  std::vector<double> X0_, Y0_, X2_, Y2_, X4_, Y4_;
  std::vector<double> X1_, Y1_, X3_, Y3_, X5_, Y5_;
  
  splineRadialFunc(const double eps, const boost::property_tree::ptree &pt) : eps_(eps) {
    greenL_ = gammaL_ = Eigen::VectorXd::Zero(MAXL);
    for (int l = 0; l < greenL_.size(); ++l) {
      greenL_[l] = l%2 == 0 ? std::pow(eps,-1 - l)*GAMMA((1 + l)/2.) : std::pow(eps,-2 - l)*GAMMA(1 + l/2.);
      gammaL_[l] = 1.0/GAMMA(1.5+l);
    }
    
    const std::string B0XY = pt.get<std::string>("BAND0_XY.value");
    ASSERT(readXY(B0XY.c_str(), X0_, Y0_) == 0);
    s0_ = std::make_shared<tk::spline>(X0_, Y0_);

    const std::string B2XY = pt.get<std::string>("BAND2_XY.value");
    ASSERT(readXY(B2XY.c_str(), X2_, Y2_) == 0);
    s2_ = std::make_shared<tk::spline>(X2_, Y2_);

    const std::string B4XY = pt.get<std::string>("BAND4_XY.value");
    ASSERT(readXY(B4XY.c_str(), X4_, Y4_) == 0);
    s4_ = std::make_shared<tk::spline>(X4_, Y4_);

    const std::string B1XY = pt.get<std::string>("BAND1_XY.value");
    ASSERT(readXY(B1XY.c_str(), X1_, Y1_) == 0);
    s1_ = std::make_shared<tk::spline>(X1_, Y1_);

    const std::string B3XY = pt.get<std::string>("BAND3_XY.value");
    ASSERT(readXY(B3XY.c_str(), X3_, Y3_) == 0);
    s3_ = std::make_shared<tk::spline>(X3_, Y3_);

    const std::string B5XY = pt.get<std::string>("BAND5_XY.value");
    ASSERT(readXY(B5XY.c_str(), X5_, Y5_) == 0);
    s5_ = std::make_shared<tk::spline>(X5_, Y5_);    
  }
  double operator()(const int l, const double r, const load_type order) const override {
    if ( order == VECTOR ) { // for point-wise loads
      if ( l == 0 ) {
        return (*s0_)(r);
      } else if ( l == 2 ) {
        return (*s2_)(r);
      } else if ( l == 4 ) {
        return (*s4_)(r);
      } else {
        return (SQRT_PI*std::pow(r,l)*greenL_[l]*gsl_sf_hyperg_1F1((1 + l)/2.,1.5 + l,-(r*r)/(eps_*eps_))*gammaL_[l])/2.;        
      }
    }

    if ( order == AFFINE ) { // for affine loads
      if ( l == 1 ) {
        return (*s1_)(r);
      } else if ( l == 3 ) {
        return (*s3_)(r);
      } else if ( l == 5 ) {
        return (*s5_)(r);
      } else {
        return SQRT_PI*std::pow(r,l)*greenL_[l]*gsl_sf_hyperg_1F1(1 + l/2.,1.5 + l,-(r*r)/(eps_*eps_))*gammaL_[l];        
      }
    }

    return std::sqrt(-1.0);
  }
};

}
#endif
