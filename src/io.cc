#include "io.h"
#include <numeric>
#include "macro.h"
#include "vtk.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

using namespace std;

namespace green {

int hex_mesh_write_to_vtk(const char *path, const matd_t &nods, const mati_t &hexs,
                          const vecd_t *mtr, const char *type) {
  ASSERT(hexs.rows() == 8);
  
  ofstream ofs(path);
  if ( ofs.fail() ) {
    return __LINE__;
  }
  
  hex2vtk(ofs, nods.data(), nods.size()/3, hexs.data(), hexs.size()/8);

  if ( mtr != nullptr ) {
    const string mtr_name = "theta";
    ofs << type << "_DATA " << mtr->size() << "\n";
    vtk_data(ofs, mtr->data(), mtr->size(), mtr_name.c_str(), mtr_name.c_str());
  }
  ofs.close();

  return 0;
}

int point_write_to_vtk(const char *path, const matd_t &nods) {
  ofstream ofs(path);
  if ( ofs.fail() )
    return __LINE__;

  matd_t nods_to_write = matd_t::Zero(3, nods.cols());
  if ( nods.rows() == 2 ) {
    nods_to_write.topRows(2) = nods;
  } else if ( nods.rows() == 3 ) {
    nods_to_write = nods;
  }

  std::vector<size_t> cell(nods.cols());
  std::iota(cell.begin(), cell.end(), 0);
  point2vtk(ofs, nods_to_write.data(), nods_to_write.cols(), &cell[0], cell.size());
  ofs.close();
        
  return 0;
}

int read_wavefront_obj(const char *filename, mati_t &cell, matd_t &nods) {
  tinyobj::ObjReaderConfig reader_config;
  tinyobj::ObjReader reader;

  if (!reader.ParseFromFile(filename, reader_config)) {
    if (!reader.Error().empty()) {
      std::cerr << "TinyObjReader: " << reader.Error();
    }
    return __LINE__;
  }

  auto& attrib = reader.GetAttrib();
  auto& shapes = reader.GetShapes();

  const size_t num_vertices = attrib.vertices.size()/3;
  nods.resize(3, num_vertices);
  std::copy(attrib.vertices.begin(), attrib.vertices.end(), nods.data());
  
  const size_t num_per_face = shapes[0].mesh.num_face_vertices[0];
  ASSERT(num_per_face == 3 || num_per_face == 4);

  std::vector<size_t> face_buf;  
  for (size_t s = 0; s < shapes.size(); s++) {
    // Loop over faces(polygon)
    size_t index_offset = 0;
    for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
      size_t fv = size_t(shapes[s].mesh.num_face_vertices[f]);
      for (size_t v = 0; v < fv; v++) {
        face_buf.emplace_back(shapes[s].mesh.indices[index_offset + v].vertex_index);
      }
      index_offset += fv;
    }
  }
  cell.resize(num_per_face, face_buf.size()/num_per_face);  
  std::copy(face_buf.begin(), face_buf.end(), cell.data());
  return 0;
}

int save_wavefront_obj(const char *filename, const mati_t &cell, const matd_t &nods) {
  std::ofstream file(filename);
  if ( file.fail() ) {
    cerr << "# cannot open " << filename << endl;
    return __LINE__;
  }

  for(int i = 0; i < nods.cols(); ++i) {
    file << std::fixed;
    file << "v " << nods(0, i) << " " << nods(1, i) << " " << nods(2, i) << std::endl;
  }

  for (int i = 0; i < cell.cols(); ++i) {
    file << "f ";
    for(int j = 0; j < cell.rows(); ++j) {
      file << cell(j, i) + 1 << " ";
    }
    file << std::endl;
  }
  
  return 0;
}

}
