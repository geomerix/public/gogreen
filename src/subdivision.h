#ifndef SUBDIVISION_H
#define SUBDIVISION_H

#include <map>
#include <Eigen/Sparse>
#include "types.h"

namespace green {

//
//          4--------6
//         /|       /|
//        5--------7 |
//        | |      | |
//        | |      | |
//        | 0------|-2
//        |/       |/
//   z    1--------3
//   |__y
//  /
// x

void collect_one_vox_edges(const veci_t &vox, std::vector<std::vector<size_t>> &edges);

void collect_one_vox_faces(const veci_t &vox, std::vector<std::vector<size_t>> &faces);

class vox_edges
{
public:
  vox_edges(const mati_t &cell);
  size_t edges_num() const;
  size_t query_edge_idx(const size_t p, const size_t q) const;
  size_t query_edge_idx(const std::vector<size_t> &ab) const;
  std::map<std::vector<size_t>, size_t> edge2idx_;
};

class vox_faces
{
public:
  vox_faces(const mati_t &cell);
  size_t faces_num() const;
  size_t query_face_idx(const size_t p, const size_t q, const size_t m, const size_t n) const;
  size_t query_face_idx(const std::vector<size_t> &abcd) const;
  std::map<std::vector<size_t>, size_t> face2idx_;
  std::map<std::vector<size_t>, std::vector<size_t>> face2vox_;
  std::map<std::vector<size_t>, std::vector<size_t>> face_in_order_;
};

int subdivide_vox(const mati_t &voxs, const matd_t &nods, mati_t &new_voxs, matd_t &new_nods);

}

#endif
