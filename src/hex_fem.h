#ifndef HEX_FEM_H
#define HEX_FEM_H

#include <Eigen/Sparse>
#include "types.h"

namespace green {

inline Vector6d sym_mat_to_voigt(const Matrix3d &E) {
  Vector6d e;
  e[0] = E(0, 0);
  e[1] = E(1, 1);
  e[2] = E(2, 2);
  e[3] = 2*E(1, 2);
  e[4] = 2*E(0, 2);
  e[5] = 2*E(0, 1);
  return e;
}

matd_t calc_cauchy_strain(const Matrix3x8d &u, const matd_t &HDmH) {
  matd_t E = matd_t::Zero(6, HDmH.cols());
  for (size_t j = 0; j < HDmH.cols(); ++j) {
    const Matrix3d &&du = u*Eigen::Map<const Matrix8x3d>(&HDmH(0, j));
    E.col(j) = sym_mat_to_voigt((du+du.transpose())/2);
  }
  return E;
}

class TwoLevHexFEM
{
 public:  
  TwoLevHexFEM(const mati_t &cell, const matd_t &nods, const matd_t &lame);
  void globalHarmonics(matd_t &hab) const;  
  Matrix6d homogC(const matd_t &hab) const;
  
 private:
  void assembleK(std::vector<Eigen::Triplet<double>> &hes) const;

 private:
  mati_t cell_h_, cell_H_;
  Eigen::Matrix3Xd nods_h_, nods_H_;
  Eigen::Matrix2Xd lame_h_;

  Eigen::Matrix3Xd qp_;
  vecd_t qw_;
  
  vecd_t detDmH_;
  matd_t HDmH_;

  double detDmH_H_;
  vecd_t HDmH_H_;
};

}
#endif
