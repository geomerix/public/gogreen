﻿# *Go Green*

This repository offers an implementation of our SIGGRAPH 2022 conference paper ["*Go Green*: General Regularized Green's Functions for Elasticity"](https://jiongchen.github.io/files/gogreen-paper.pdf).

## Environment

The project was developed and tested on `Ubuntu 20.04` with `gcc 9.3.0`. Following packages are required to compile the code.

```
apt install libopencv-dev gfortran libgsl-dev libboost-all-dev
```
## Compilation 

```
 mkdir build; cd build; cmake -DCMAKE_BUILD_TYPE=Release ..
```

## Examples

You can run several examples from `makefiles/` subdirectory by executing bash scripts there. A python interface `makefiles/edit_curve.py` is provided to edit cubic splines for each degree of spherical harmonics. It saves positions of knots to ASCII files `makefiles/band-%d.xy`, which are used as inputs for C++ programs. See `makefiles/pipeline.mk` for the organization of this project.

## Contact

Author: [Jiong Chen](https://jiongchen.github.io/)<br>
Email: jiong.chen@inria.fr

