project(greenfunc)
cmake_minimum_required(VERSION 2.8)

list(APPEND CMAKE_CXX_FLAGS "-fopenmp -std=c++14 -fpermissive -march=native -ffast-math -DNDEBUG")
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/;${CMAKE_MODULE_PATH}")

include_directories(${PROJECT_SOURCE_DIR})

# BOOST
find_package(Boost REQUIRED)
if(Boost_FOUND)
  message("-- Boost @ ${Boost_INCLUDE_DIRS}")
  message("-- Boost verison ${Boost_VERSION}")
  include_directories(${Boost_INCLUDE_DIRS})  
endif(Boost_FOUND)

# GSL
find_package(GSL REQUIRED)
if(GSL_FOUND)
  message("-- GSL @ ${GSL_INCLUDE_DIRS}")
  include_directories(${GSL_INCLUDE_DIRS})
endif(GSL_FOUND)

# OPENCV
find_package(OpenCV REQUIRED)
if(OpenCV_FOUND)
  message(${OpenCV_LIBS})
  include_directories(${OpenCV_INCLUDE_DIRS})
endif(OpenCV_FOUND)

# HEADER-ONLY LIBS
include_directories(external/eigen/)
include_directories(external/spdlog/include)
include_directories(external/libigl/include)

add_subdirectory(src)
add_subdirectory(examples)
