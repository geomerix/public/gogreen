#!/usr/bin/python

import os
import sys

filename = sys.argv[1]
output = filename[:-4]+'.png'

print(output)

if not os.path.exists(output):

    from paraview import simple
    from paraview.simple import *

    input_mesh = filename
    print(input_mesh)
    
    reader = simple.OpenDataFile(input_mesh)

    simple.Show(reader)
    simple.SetDisplayProperties(Representation = "Surface With Edges")
    view = simple.GetActiveView()
    Show()
    view.Background = [1,1,1]  #white

    # set image size
    view.ViewSize = [720, 480]

    Render()

    # save screenshot
    WriteImage(output)

    Delete(reader)
